const express = require('express');
const app = express();
app.use(express.static('public'));
app.set('port', process.env.port || 3300)
    .set('views', __dirname + '/views');

const port = app.get('port');
const server = app.listen(port, () => {
    console.log('Server up', port);
});